Paper can be found here:  https://www.nature.com/articles/s42003-023-05548-w

We used custom Matlab codes for metabolic model generation and model analysis and R script for data analysis and visualization:

DEGS_IPD.Rmd : for analysis of RNA sequencing data

Flux_change_visualisation.Rmd : visualisation of flux changes estimated in metabolic model analysis

Integration.Rmd : Transcriptomic, metabolomic and Seahorse assay data Integration

KEGG_pathway_identification.Rmd : gene set enrichment analysis using KEGG database

NESC_non_polar_mets_untargeted_analysis.Rmd : metabolomics data anlaysis of non polar fraction 

NESC_polar_mets_untargeted_analysis.Rmd : metabolomics data analysis of polar fraction

Plotting_mitoS1.Rmd : visualisation of results from metabolic microarray MitoPlate S-1 after data preprocessing with Data Analysis software (version 1.7.)
