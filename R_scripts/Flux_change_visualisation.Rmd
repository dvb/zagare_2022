---
title: "NESC_nonPolar_mets_analysis_Final"
author: "Alise"
date: "2020 M03 14"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
library(tidyverse)
library(pheatmap)
library(RColorBrewer)
```


#All reactions
```{r}
#setwd("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/originals/models/NESCs_paper/NESCs_paper/results")

rxns_all <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/A/results/allChange.csv", header=T, row.names=1, sep=",")
```

```{r}
rxns_all2<-rxns_all[,4:9]
```

```{r}
#colnames(data3)<-c('Ctrl_1', 'Ctrl_2', 'Ctrl_3', 'IPD_1', 'IPD_2', 'IPD_4', 'IPD_3', 'IPD_5', 'IPD_6')
colnames(rxns_all2)<-c('CTRL1', 'CTRL2', 'CTRL3','IPD1', 'IPD2', 'IPD3')


pheatmap(as.matrix(rxns_all2), color = colorRampPalette(rev(brewer.pal(n = 8, name = "Spectral")))(100),legend=TRUE, show_rownames = T, scale='row', main="Top 50 reactions", angle_col = 45, fontsize_col=10,fontsize_row = 6, cellwidth = 20,cellheight = 5, display_numbers = F, fontsize_number = 4)

```

```{r}
rxns_allgr <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/B/Rxns_all_grouped_perSubsystem.csv", header=T, sep=",")
colnames(rxns_allgr)<-c('Subsystem', 'Reactions')
rxns_allgr<-rxns_allgr[order(-rxns_allgr$Reactions),]
top5<-rxns_allgr[1:5,]
```


```{r}
ggplot(top5) +
  geom_bar(aes(x = Subsystem, y = Reactions, fill=Subsystem),
           stat = "identity", position = "dodge", width=0.3) +
   theme_classic()+
  scale_fill_manual(values = c("cyan3","#72a6ad", "#9DBEBB","#a1cbd1","darkcyan"))+ 
  labs(title="Top 5 subsystems", y="Reaction count", x="")+
  coord_flip()+
  theme_bw()+
  theme(axis.text.y = element_text(size=22), axis.text.x=element_text(size=22), axis.title.x=element_text(size=22), legend.position = "none", title=element_text(size=26))
```
```{r}
rxns_allgrPath <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/B/Rxns_all_grouped_perPathway.csv", header=T, sep=",")
colnames(rxns_allgrPath)<-c('Pathway', 'Reactions')
rxns_allgrPath<-rxns_allgrPath[order(-rxns_allgrPath$Reactions),]
#top5<-rxns_allgr[1:5,]
```


```{r}
g <- ggplot(rxns_allgrPath, aes(x=reorder(Pathway,Reactions), y=Reactions, fill=Pathway)) +
       geom_bar(stat = "identity", width=0.4)+
       #geom_text(aes(label=Subsystem), size=3, position=position_stack(vjust=.5)) +
       #scale_y_continuous(breaks=seq(0, 100, by=10)) +
       labs(title="Most dysregulated pathways", y="Reaction count", x="")+
  coord_flip()+
  theme_bw()+
  theme(axis.text.y = element_text(size=20), axis.text.x=element_text(size=16), axis.title.x=element_text(size=16), legend.position = "none", title=element_text(size=20))
g
```
```{r}
pie <- ggplot(rxns_allgrPath, aes(x="", y=Reactions, fill=Pathway)) + 
scale_fill_manual(values= c("plum", "#EFC000FF", "#CD534CFF","#0073C2FF")) +
geom_bar(stat="identity", width=1) + coord_polar("y", start=0) + geom_text(aes(label = paste0(round(Reactions), "%")), position = position_stack(vjust = 0.5))+
labs(x = NULL, y = NULL, fill = NULL, title = "Most dysregulated pathways")+
theme_classic() + theme(axis.line = element_blank(),
          axis.text = element_blank(),
          axis.ticks = element_blank(),
          plot.title = element_text(hjust = 0.5, color = "black"),
          title=element_text(size=18))
  
  
 pie
```
#Mitochondrial and NAD reactions
```{r}
rxns_nad <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/D/nadChange.csv", header=T, row.names=1, sep=",")
#or
rxns_nad <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/D/nadCitoFlux.csv", header=T, row.names=1, sep=",")
or
rxns_nad <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/D/nadMitoFlux.csv", header=T, row.names=1, sep=",")
```

```{r}
rxns_nad2<-rxns_nad[,4:9]
```

```{r}
#colnames(data3)<-c('Ctrl_1', 'Ctrl_2', 'Ctrl_3', 'IPD_1', 'IPD_2', 'IPD_4', 'IPD_3', 'IPD_5', 'IPD_6')
colnames(rxns_nad2)<-c('CTRL1', 'CTRL2', 'CTRL3','IPD1', 'IPD2', 'IPD3')


pheatmap(as.matrix(rxns_nad2), color = colorRampPalette(rev(brewer.pal(n = 8, name = "Spectral")))(100),legend=TRUE, show_rownames = T, scale='row', main="NAD+ consumption in cytosol", angle_col = 45, fontsize_col=10,fontsize_row = 6, cellwidth = 20,cellheight = 5, display_numbers = F, fontsize_number = 4)

```






```{r}
rxns_mito <- read.csv("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure3_modeling/partials/E/oxPhoTcaAtpmFlux.csv", header=T, row.names=1, sep=",")
```

```{r}
rxns_mito2<-rxns_mito[,4:9]
```

```{r}
#colnames(data3)<-c('Ctrl_1', 'Ctrl_2', 'Ctrl_3', 'IPD_1', 'IPD_2', 'IPD_4', 'IPD_3', 'IPD_5', 'IPD_6')
colnames(rxns_mito2)<-c('CTRL1', 'CTRL2', 'CTRL3','IPD1', 'IPD2', 'IPD3')


pheatmap(as.matrix(rxns_mito2), color = colorRampPalette(rev(brewer.pal(n = 8, name = "Spectral")))(100),legend=TRUE, show_rownames = T, scale='row', main="TCA and OXPHOS", angle_col = 45, fontsize_col=10,fontsize_row = 6, cellwidth = 20,cellheight = 5, display_numbers = F, fontsize_number = 4)

```



