---
title: "NESC_nonPolar_mets_analysis_Final"
author: "Alise"
date: "2020 M03 14"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r}
setwd("Z:/16-Our Papers/In Preparation/IPD_metabolism _ Alise/Figures/Figure1/originals/Non-Polar_mets")
getwd()
 
metdata <- read.csv("hNESCs_non_polar_GC_MS2.csv", header=T, row.names=1, sep=",")
```


```{r}
library(tidyverse)
library(pheatmap)
library(RColorBrewer)
```

```{r}

data<- metdata %>% mutate(Ctrl_39 = as.numeric(apply(metdata[,1:3],1,median)))%>% 
  mutate(Ctrl_48 = as.numeric(apply(metdata[,4:6],1,median)))%>%
  mutate(Ctrl_56 = as.numeric(apply(metdata[,7:9],1,median)))%>%
  mutate(IPD_12 = as.numeric(apply(metdata[,10:12],1,median)))%>%
  mutate(IPD_13 = as.numeric(apply(metdata[,13:15],1,median)))%>%
  mutate(IPD_2490 = as.numeric(apply(metdata[,16:18],1,median)))%>%
  mutate(IPD_28 = as.numeric(apply(metdata[,19:21],1,median)))%>%
  mutate(IPD_30C2 = as.numeric(apply(metdata[,22:24],1,median)))%>%
  mutate(IPD_30C9 = as.numeric(apply(metdata[,25:27],1,median)))

```

```{r}
data2<-data[,c(28:32, 34)]
#column_to_rownames(data2,var="Metabolite")->data3
```

```{r}
#colnames(data3)<-c('Ctrl_1', 'Ctrl_2', 'Ctrl_3', 'IPD_1', 'IPD_2', 'IPD_4', 'IPD_3', 'IPD_5', 'IPD_6')
colnames(data2)<-c('CTRL_1', 'CTRL_2', 'CTRL_3', 'IPD_1', 'IPD_2', 'IPD_3')

Z_Normalize<-function(x){return((x-mean(x))/(sd(x)))}

NormalisedData_Zscore = t(apply(as.matrix(data2),1,Z_Normalize))

heatmap(as.matrix(NormalisedData_Zscore))

pheatmap(as.matrix(NormalisedData_Zscore), color = colorRampPalette(rev(brewer.pal(n = 8, name = "Spectral")))(100),legend=TRUE, show_rownames = F, scale='row', main="Non-polar intracellular metabolites", angle_col = 45, fontsize_col=10,cellwidth = 30, width=30, height = 15)

```

```{r}
Ctrl <- data2[grepl("CTRL", names(data2))]
iPD <- data2[grepl("IPD", names(data2))]
```


###test with Welchs corection
```{r}
pvalue=sapply(1:nrow(data2),function(i)
{t.test(Ctrl[i,],iPD[i,])$p.value}
)
FC <- apply(iPD,1,mean)/apply(Ctrl,1,mean)
FDR <- p.adjust(pvalue, method = "BH")

CtrlvsiPD = cbind(FC, pvalue, FDR) 
write.csv(CtrlvsiPD,file="Ttest_NP.csv")

CtrlvsiPD<-data.frame(CtrlvsiPD)
Significance<-CtrlvsiPD[with(CtrlvsiPD,order(pvalue)),]
head(Significance,10)
#write.csv(Significance, 'Ttest_hNESCs_NonPolar.csv')
```


###PCA
```{r}
library(factoextra)
tCtrl_iPD <- t(data2)
res.pca <- prcomp(tCtrl_iPD, scale = TRUE)
summary(res.pca)
fviz_eig(res.pca, addlabels = TRUE, ylim = c(0, 70)) ### to see the importance of each component

```


```{r}
fviz_pca_ind(res.pca,
             col.ind = "cos2", # Color by the quality of representation
             gradient.cols = c("#00AFBB", "#E7B800", "#FC4E07"),
             repel = TRUE )    # Avoid text overlapping
```



```{r}
Condition <- as.factor(c(rep("CTRL",3),rep("IPD",3)))
g <- fviz_pca_ind(res.pca, 
             col.ind = Condition, # color by groups
             palette = c("#48B368",  "#FC4E07"),
             addEllipses = TRUE, # Concentration ellipses
             ellipse.type = "confidence",
             legend.title = "Condition",
             geom.ind =  "text",
             labelsize=8,
             repel = TRUE
             )
ggpubr::ggpar(g,
              font.ytickslab = 24, font.xtickslab = 24, font.x = 24, font.y = 24, font.legend = 24, title="")
            
```


```{r}
###creating a Volcano plot

df <- as.data.frame(CtrlvsiPD)
df1 <- as.data.frame(log(df$FC,2))
df2 <- as.data.frame(-log(df$FDR,10))


mets <- rownames(metdata)
data <- cbind.data.frame(mets,df,df1)
colnames(data) <- c("mets","FC", "pvalue", "FDR", "log2FoldChange")

```


```{r}
library(ggrepel)
data$Significant <- ifelse(data$pvalue < 0.05, "pvalue < 0.05", "Not significant")
ggplot(data, aes(x = log2FoldChange, y = -log10(pvalue))) +
  geom_point(aes(color = Significant), size=4) +
  scale_color_manual(values = c("black", "red")) +
    #labs( x = "log2FoldChange")+
  theme_bw(base_size = 20) + theme(legend.position = c(0.75,0.85), legend.title = element_blank(), axis.title.x=element_text(size = 28), 
axis.title.y=element_text(size=28), legend.text=element_text(size=24), text=element_text(size=34))
  
```

```{r}
library(ggplot2)
tCtrl_iPD %>% as_tibble(tCtrl_iPD, rownames='Labels') %>%
  mutate(Condition = case_when(str_detect(Labels, 'IPD') ~ 'IPD', str_detect(Labels, 'CTRL') ~ 'Ctrl',
                             TRUE ~ 'test'))  -> metdataT
```
```{r}
###Glycerol
my_sum <- metdataT %>%
  group_by(Condition) %>%
  summarise( 
    n=n(),
    mean=mean(Glycerol_3TMS),
    sd=sd(Glycerol_3TMS)
  ) %>%
  mutate( se=sd/sqrt(n))  %>%
  mutate( ic=se * qt((1-0.05)/2 + .5, n-1))

t.test(Glycerol_3TMS~Condition, data=metdataT)

ggplot(my_sum) +
    geom_bar(aes(x = Condition, y = mean, fill=Condition), stat="identity",alpha=1, width = 0.6) +
  scale_fill_manual(values=c('dark grey', 'grey38'))+
    geom_errorbar( aes(x = Condition, ymin = mean-sd, ymax=mean+sd), width=0.1, colour="black", alpha=0.9, size=1.3)+
  theme_classic()+
    labs(title="Glycerol", y = "Relative abundance")+
    theme(plot.title=element_text(size=32, face="bold", hjust = 0.1),
        axis.text.x=element_text(size = 30, face = "bold"), 
        axis.title.x=element_blank(), axis.title.y=element_text(size=34),axis.text.y=element_text(size=30), legend.text = element_text(size=30), legend.title = element_text(size=30))
```

