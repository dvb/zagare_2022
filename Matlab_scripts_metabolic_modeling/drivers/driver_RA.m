% This driver preform robustnes analisis of reactions of interest by varing
% the ATP demand

clear

% Prepare and load data
projectDir = fileparts(which('hNESC_modelling.mlx'));
resultsFolder = [projectDir filesep 'results' filesep];

% Robustnes analisis parameters
inSilicoLines = {'CTRL39', 'CTRL48', 'CTRL56', 'IPD12', 'IPD13', 'IPD28'};
rxnsOfInterest = {'PGK'; 'PYK'; 'ATPtm'; 'LTDCL'; 'NMNATm'; 'NMNATr'; 'NADS2'; 'NADtm'};
range = 186:186 * 10;
npoints = 100;
bound = 'l';
varRxn = 'ATPM';
objective = {'unWeightedTCBMflux'};
param.objectives = objective;
param.printLevel = 0;
param.tests = {'flux'};

% Make a subplot 
[rx1, rx2] = deal(zeros(npoints, 1));
boundsToTest = min(range):(max(range) - min(range)) / npoints:max(range);
figure('visible', 'on');
for i = 1:length(rxnsOfInterest)
    
    for k = 1:length(inSilicoLines)
        
        load([resultsFolder 'models' filesep inSilicoLines{k} filesep 'Model.mat'])
        rxnIds = findRxnIDs(Model, rxnsOfInterest);
        
        for j = 1:length(boundsToTest)
            
            % Robustnes analisis
            model = Model;
            model = changeRxnBounds(model, varRxn, boundsToTest(j), bound);
            solutions = modelMultipleObjectives(model, param);
            rx1(j, k) = boundsToTest(j);
            rx2(j, k) = solutions.(objective{:}).v(rxnIds(i));
            
        end
        % default flux
        solutions.(objective{:}).v(rxnIds(i))
        scatter(Model.lb(rxnIds(i)), solutions.(objective{:}).v(rxnIds(i)), 'filled')        
        
        sgtitle({'Phenotype of essential', 'reactions when varying ATP demand'}, 'FontSize', 16)
        subplot(4, 2, i)
        hold on
        plot(rx1(:, k), rx2(:, k), 'LineWidth', 2)
        axis([186 186*10 min(rx2(:, k)) max(rx2(:, k))])
        xlabel([varRxn ' umol/gDW/hr'], 'FontSize', 12)
        ylabel([rxnsOfInterest{i} ' umol/gDW/hr'], 'FontSize', 12)
        title({model.rxnNames{rxnIds(i)}, model.rxnFormulas{rxnIds(i)}}, 'FontSize', 14)
        if i == 1
            legend(inSilicoLines, 'Location', 'best', 'FontSize', 12)
        end
        
    end
    hold off
end

savefig([resultsFolder  'rba'])
saveas(gcf,[resultsFolder  'rba'],'png')
saveas(gcf,[resultsFolder  'rba'],'eps')

% tryptophan
%   LTDCL  {'h[c] + trp_L[c]  -> co2[c] + trypta[c] '      } 1.00767810814473e-05
%   r1547  {'gly[e] + trp_L[c]  <=> gly[c] + trp_L[e] '    } 0.537185620241689
%   r1561  {'ala_L[e] + trp_L[c]  <=> ala_L[c] + trp_L[e] '} -2.25546757106607
%   r1586  {'ser_L[e] + trp_L[c]  <=> ser_L[c] + trp_L[e] '} 2.06887839330622
%   r1597  {'trp_L[c] + met_L[e]  <=> met_L[c] + trp_L[e] '} 0.192036437074183
%   r1608  {'phe_L[c] + trp_L[e]  <=> trp_L[c] + phe_L[e] '} -0.253453119464631
%   r1609  {'tyr_L[c] + trp_L[e]  <=> trp_L[c] + tyr_L[e] '} -0.337138734184245
%   r1610  {'cys_L[c] + trp_L[e]  <=> cys_L[e] + trp_L[c] '} -0.27777142038532
%   r1612  {'pro_L[c] + trp_L[e]  <=> trp_L[c] + pro_L[e] '} -0.236331601486837
%   r1614  {'trp_L[e] + val_L[c]  <=> trp_L[c] + val_L[e] '} -0.479966216665025
%   r1615  {'thr_L[c] + trp_L[e]  <=> thr_L[e] + trp_L[c] '} -0.383354645506359
%   r1616  {'hom_L[c] + trp_L[e]  <=> hom_L[e] + trp_L[c] '} -0.210073693131176
%   r1617  {'ile_L[c] + trp_L[e]  <=> ile_L[e] + trp_L[c] '} -0.278246908377438
%   TRPt  {'trp_L[e]  <=> trp_L[c] '                      } 2.99897929502947
