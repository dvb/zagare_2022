% Generate the control and IPD models model conditions with different conditions using the
% function XomicsToMultipleModels. The current conditions are divided in 9
% groups:
%
%   1. Generic model: noLumpedRxns - wLumpedRxns
%   2. cobraSolver: gurobi - ibm_cplex
%   3. activeGenesApproach: deleteModelGenes - oneRxnPerActiveGene
%   4. transcriptomicThreshold: [0 2]
%   5. closeIons: [true false]
%   6. tissueSpecificSolver: fastCore - thermoKernel
%   7. inactiveGenesTranscriptomics: [true false]
%   8. limitBounds: [1e3 1e4]
%
% The models are saved in:
% ~/work/sbgCloud/programReconstruction/projects/exoMetDN/results/codeResults/iDN1
%% Set directories

clear

projectDir = fileparts(which('hNESC_modelling.mlx'));
dataFolder = [projectDir filesep 'data' filesep];
resultsFolder = [projectDir filesep 'results' filesep];
modelsFolder = [resultsFolder 'models' filesep];
addpath([projectDir filesep 'drivers'])

% Define results directory
modelGenerationConditions.outputDir = modelsFolder;

%% Load generic model

genericModelName = 'Recon3DModel_301_xomics_input.mat';
load([dataFolder filesep genericModelName])
modelGenerationConditions.genericModel.Recon3D = model;

%% Prepare bibliomic data

% Read bibliomics data
bibliomicData = 'bibliomicData.xlsx';
specificData = preprocessingOmicsModel([dataFolder bibliomicData], 1, 1);

% Delete non-human references
% Genes
activeGenes = readtable([dataFolder bibliomicData], 'Sheet', 'activeGenes');
rowsToKeep = cellfun(@isempty, regexp(activeGenes.Notes, ' rat | rats | mouse | mice | rodent | rodents'));
activeGenes = activeGenes(rowsToKeep, :);
specificData.activeGenes = cellstr(num2str(activeGenes.geneID));
% Reactions
activeReactions = readtable([dataFolder bibliomicData], 'Sheet', 'activeReactions');
rowsToKeep = cellfun(@isempty, regexp(activeReactions.notes, ' rat | rats | mouse | mice | rodent | rodents'));
activeReactions = activeReactions(rowsToKeep, :);
specificData.activeReactions = activeReactions.rxnID;
% inactiveGenes
inactiveGenes = readtable([dataFolder bibliomicData], 'Sheet', 'inactiveGenes');
rowsToKeep = cellfun(@isempty, regexp(inactiveGenes.notes, ' rat | rats | mouse | mice | rodent | rodents'));
inactiveGenes = inactiveGenes(rowsToKeep, :);
specificData.inactiveGenes = cellstr(num2str(inactiveGenes.geneID));

%% Transcriptomics data

% Vary transcriptomic data
transcriptomicData = readtable([dataFolder 'All_genes_TPM_EntrezID.csv']);
modelsLabels = ['CTRLmean'; transcriptomicData.Properties.VariableNames(~cellfun(@isempty, ...
    regexp(transcriptomicData.Properties.VariableNames, 'CTRL|IPD')))'];

for i = 1:length(modelsLabels)
    
    % Add trascriptomic data per each cell line
    modelGenerationConditions.specificData.(modelsLabels{i}) = specificData;
    modelGenerationConditions.specificData.(modelsLabels{i}).transcriptomicData = table;
    modelGenerationConditions.specificData.(modelsLabels{i}).transcriptomicData.genes = transcriptomicData.ENTREZID;
    
    if i == 1
        meanExpVal = mean([transcriptomicData.(modelsLabels{2}) transcriptomicData.(modelsLabels{3}) transcriptomicData.(modelsLabels{4})], 2);
        modelGenerationConditions.specificData.(modelsLabels{i}).transcriptomicData.expVal = meanExpVal;
    else
        modelGenerationConditions.specificData.(modelsLabels{i}).transcriptomicData.expVal = transcriptomicData.(modelsLabels{i});
    end
end

%% Cell media uptake

% Cell culture data
volume = 0.0020; %(L)
interval = 24; %(hr)
assayVolume = 5e-6; %(L)
proteinFraction = 0.7060; %dimensionless
uptakeSign = -1; %dimensionless

% Meassured data
averageProteinConcentrations = table;
averageProteinConcentrations.lines = modelsLabels;
averageProteinConcentrations.concentration = [mean([1.247; 0.5475; 0.46]); ...
    1.247; 0.5475; 0.46; 2.3255; 1.4545; 0.721];

% Asssign max media uptake for each averageProteinConcentrations
for i = 1:length(modelsLabels)
    
    modelGenerationConditions.specificData.(modelsLabels{i}).mediaData.mediumMaxUptake = [];
    
    for j = 1:length(specificData.mediaData.rxnID)
        % options.mediumConcentrations(i) *
        % (uptakeSign * volume(L) * proteinFraction) /
        % (interval (hr) * averageProteinConcentration (gDW/L) * assayVolume(L))
        modelGenerationConditions.specificData.(modelsLabels{i}).mediaData.mediumMaxUptake(j) = ...
            modelGenerationConditions.specificData.(modelsLabels{i}).mediaData.mediumConcentrations(j) * ...
            (uptakeSign * volume * proteinFraction) / ...
            (interval * averageProteinConcentrations.concentration(i) * assayVolume);
    end
end

%% Fixed options

% Parameters tested
param.tissueSpecificSolver = 'thermoKernel';
param.activeGenesApproach = 'oneRxnPerActiveGene'; % {'deleteModelGenes', 'oneRxnPerActiveGene'};
param.transcriptomicThreshold = 2; % [0 1 2];
param.TolMinBoundary = -1e5;
param.TolMaxBoundary = 1e5;
param.inactiveGenesTranscriptomics = true; % [true false];
param.closeIons = true; % [true false];
param.curationOverOmics = false;

% General parameters
param.workingDirectory = modelsFolder;
param.printLevel = 2;
param.setObjective = ''; % No objective function
feasTol = getCobraSolverParams('LP', 'feasTol');
param.boundPrecisionLimit = feasTol * 10;
param.fluxEpsilon = feasTol * 10;
param.fluxCCmethod = 'fastcc';
param.weightsFromOmics = 1;
param.metabolomicWeights = 'mean';
%param.sinkDMinactive = 1; % Set non-core sinks and demands to inactive
param.addCoupledRxns = 1;
param.nonCoreSinksDemands = 'closeAll';
param.closeUptakes = true; % Cell culture information
param.debug = true;

%% Create models

% Remove expressionRxns
if isfield(model, 'expressionRxns')
    model = rmfield(model, 'expressionRxns');
end

% Create models
replaceModels = false;
directoriesWithModels = XomicsToMultipleModels(modelGenerationConditions, param);

display('Models saved in:')
display(projectDir)

%% Models dimensions

% Create table
nRows = length(modelsLabels);
varTypes = {'string', 'double', 'double', 'double', 'double'};
varNames = {'Lines', 'Metabolites', 'Reactions', 'Genes', 'Rank'};
modelsDimensions = table('Size', [nRows length(varTypes)], 'VariableTypes', varTypes,...
    'VariableNames', varNames);

% Add data for each model
for i = 1:length(modelsLabels)
    load([modelsFolder modelsLabels{i} filesep 'Model.mat'])
    modelsDimensions.Lines(i) = modelsLabels{i};
    modelsDimensions.Metabolites(i) = length(Model.mets);
    modelsDimensions.Reactions(i) = length(Model.rxns);
    modelsDimensions.Genes(i) = length(Model.genes);
    modelsDimensions.Rank(i) = rank(full(Model.S));
end

display(modelsDimensions)