% This driver uses the Spearman correlation to test the topological 
% similarity of the in silico cell lines by comparing their metabolites, reactions, and genes.
%
% results saved in:
% /results/corr_genes.png
% /results/corr_genes.fig
% /results/corr_mets.png
% /results/corr_mets.fig
% /results/corr_rxns.png
% /results/corr_rxns.fig
%% List of metabolites reactions and genes in the models

clear

% Get labels and directories
projectDir = fileparts(which('hNESC_modelling.mlx'));
% dataFolder = [projectDir filesep 'data' filesep];
resultsFolder = [projectDir filesep 'results' filesep];
modelsFolder = [resultsFolder 'models' filesep];
modelsLabels = dir(modelsFolder);
modelsLabels = struct2cell(modelsLabels([modelsLabels.isdir]));
modelsLabels = modelsLabels(1, 3:end)';

% Create the tables
varTypes = {'string', 'logical', 'logical', 'logical', 'logical', 'logical', 'logical'};
varNames = ['IDs', modelsLabels'];
elements.mets = table('Size', [0 length(varTypes)], 'VariableTypes', varTypes,...
    'VariableNames', varNames);
elements.rxns = elements.mets;
elements.genes = elements.mets;

% True value per each element present in the corresponding model
warning('off')
elements2test = fieldnames(elements);
for j = 1:length(elements2test)
    for i = 1:length(modelsLabels)
        load([modelsFolder modelsLabels{i} filesep 'Model.mat'])
        % List of elements
        element2add = setdiff(Model.((elements2test{j})), elements.(elements2test{j}).IDs);
        elements.(elements2test{j}).IDs(height(elements.(elements2test{j})) + 1: height(elements.(elements2test{j})) + length(element2add)) = element2add;
        elements.(elements2test{j}).(modelsLabels{i}) = ismember(elements.(elements2test{j}).IDs, Model.(elements2test{j}));
    end
end
warning('on')

% Sort each of the rows
elements.mets = sortrows(elements.mets);
elements.rxns = sortrows(elements.rxns);
elements.genes = sortrows(elements.genes);

% Compare elements and save figures
elementsTested = {'mets', 'rxns', 'genes'};
for i = 1:length(elementsTested)
    elementsCorrelation.(elementsTested{i}) = zeros(length(modelsLabels));
    for j = 1:length(modelsLabels)
        for k = 1:length(modelsLabels)
            elementsCorrelation.(elementsTested{i})(j, k) = corr(elements.(elementsTested{i}).(modelsLabels{j}), elements.(elementsTested{i}).(modelsLabels{k}),'Type','Spearman');
        end
    end
    cgo = clustergram(elementsCorrelation.(elementsTested{i}), 'Colormap', redbluecmap);
    cgo.Annotate = true;
    set(cgo, 'RowLabels', modelsLabels, 'ColumnLabels', modelsLabels)
    title = addTitle(cgo, ['Spearman correlation of ' elementsTested{i}]);
    savefig([resultsFolder 'corr_' elementsTested{i}])
    f = figure('visible','off');
    saveas(plot(cgo, f), [resultsFolder 'corr_' elementsTested{i}], 'png')
end