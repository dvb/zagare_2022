% This driver estimates the flux of each line in silico using the objective
% function unweighted thermodynamic constraint based modeling for fluxes
% (unWeightedTCBMflux) to compare the fold change in order to cluster them
%
% results saved at:
% ~/results/changeAll.csv
% ~/results/changeAll.fig
% ~/results/changeAll.png
% ~/results/clusterrxnsOfInterest.csv
% ~/results/clusterrxnsOfInterest.fig
% ~/results/clusterrxnsOfInterest.png
% ~/results/rxnFluxes.mat
%% Set up the driver

clear

% Set directories
projectDir = fileparts(which('hNESC_modelling.mlx'));
resultsFolder = [projectDir filesep 'results' filesep];

% Set the tags for the in silico lines
inSilicoLines = {'CTRL39', 'CTRL48', 'CTRL56', 'IPD12', 'IPD13', 'IPD28'};

%% Solve for each model

if ~isfile([resultsFolder 'rxnFluxes.mat'])
    
    % Make table
    nRows = 0;
    varTypes = {'string', 'string', 'string', 'string', 'double', 'double', 'double', 'double', 'double', 'double', 'double'}; % Fixed variables for the tables
    varNames = {'rxns', 'rxnNames', 'rxnFormulas', 'subSystems', 'CTRL39', 'CTRL48', 'CTRL56', 'CTRLmean', 'IPD12', 'IPD13', 'IPD28'};
    rxnFluxes = table('Size', [nRows length(varTypes)], 'VariableTypes', varTypes, 'VariableNames', varNames);
    
    % Solve and save the fluxes in the table
    for i = 1:length(inSilicoLines)
        
        % Load model
        load([resultsFolder 'models' filesep inSilicoLines{i} filesep 'Model.mat'])
        
        % Solve
        param.printLevel = 0;
        param.objectives = {'unWeightedTCBMflux'; 'unWeightedTCBMfluxConc'};
        solution = modelMultipleObjectives(Model, param);
        
        % Look for existing reactions in the table, if a reaction is not
        % present it is generated a new row.
        [boolRxns, locb] = ismember(Model.rxns, rxnFluxes.rxns);
        if any(~boolRxns)
            tableDataIdxs = length(rxnFluxes.rxns) + 1:length(rxnFluxes.rxns) + sum(~boolRxns);
            rxnFluxes.rxns(tableDataIdxs) = Model.rxns(~boolRxns);
            rxnFluxes.rxnNames(tableDataIdxs) = Model.rxnNames(~boolRxns);
            rxnFluxes.rxnFormulas(tableDataIdxs) = Model.rxnFormulas(~boolRxns);
            rxnFluxes.subSystems(tableDataIdxs) = Model.subSystems(~boolRxns);
            [~, locb] = ismember(Model.rxns, rxnFluxes.rxns);
        end
        rxnFluxes.(inSilicoLines{i})(locb) = solution.(param.objectives{1}).v;
        
    end
    
    % save fluxes
    save([resultsFolder 'rxnFluxes'], 'rxnFluxes')
    
else
    load([resultsFolder 'rxnFluxes.mat'])
end

%% Sort
% Calculate percentage difference mean to test the similarity using the 
% control fluxes and each model and sort the reactions based on the
% greatest change

% Calculate control mean
inSilicoLinesBool = ismember(rxnFluxes.Properties.VariableNames, inSilicoLines);
fluxMatrix = table2array(rxnFluxes(:, inSilicoLinesBool));
fluxControlMean = mean(fluxMatrix(:, 1:3), 2);

% Test the change using different approaches:
%   foldChangeMatrixMean: flux / meanCtrlFlux; excluding line ctrlMean
%   percentageDifferenceMean: (flux - meanCtrlFlux) / meanCtrlFlux
[noOfRxns, noOfLines] = size(fluxMatrix);
[percentageDifferenceMean, foldChangeMatrixMean] = deal(zeros(noOfRxns, noOfLines));
for i = 1:noOfLines
    foldChangeMatrixMean(:, i) = (fluxMatrix(:, i) ./ fluxControlMean);
    percentageDifferenceMean(:, i) = ((fluxMatrix(:, i) - fluxControlMean)) ./ fluxControlMean;
end

% Sort based on the percentageDifferenceMean all the fluxes based on their
% percentageDifferenceMean
sumAbsFluxes = nansum(abs(percentageDifferenceMean), 2) ./ length(inSilicoLines);
sumAbsFluxes(isinf(sumAbsFluxes)) = 0;
[~, ia] = sort(sumAbsFluxes, 'descend');
rxnFluxes = rxnFluxes(ia, :);
percentageDifferenceMean = percentageDifferenceMean(ia, :);

%% CLUSTER AND SAVE REACTIONS OF INTEREST

% Select the type of cluster
typeOfData = 'mito';
switch typeOfData
    case 'all'
        rxnsOfInterest = ones(height(rxnFluxes), 1);
    case 'nadOxPhoTca'
        % Identify relevant reactions
        rxnsOfInterest = [];
        for i = 1:noOfLines
            % Load model
            load([resultsFolder 'models' filesep inSilicoLines{i} filesep 'Model.mat'])
            rxnsOfInterest = [rxnsOfInterest; Model.rxns(ismember(Model.subSystems, ...
                {'NAD metabolism'; 'Oxidative phosphorylation'; 'Citric acid cycle'}))];
        end
        rxnsOfInterest = ismember(rxnFluxes.rxns, unique(rxnsOfInterest));
    case 'oxPhoTcaAtpm'
        rxnsOfInterest = [];
        for i = 1:noOfLines
            % Load model
            load([resultsFolder 'models' filesep inSilicoLines{i} filesep 'Model.mat'])
            rxnsOfInterest = [rxnsOfInterest; Model.rxns(ismember(Model.subSystems, ...
                {'Oxidative phosphorylation'; 'Citric acid cycle'})); 'ATPM'];
        end
        rxnsOfInterest = ismember(rxnFluxes.rxns, unique(rxnsOfInterest));
    case 'nadCito'
        % Set metabolites of interest
        metabolitesOfInterest = {...
            'nad[c]'};
        
        % Identify relevant reactions
        rxnsOfInterest = [];
        for i = 1:noOfLines
            % Load model
            load([resultsFolder 'models' filesep inSilicoLines{i} filesep 'Model.mat'])
            rxnsOfInterest = [rxnsOfInterest; findRxnsFromMets(Model, metabolitesOfInterest)];
        end
        rxnsOfInterest = ismember(rxnFluxes.rxns, unique(rxnsOfInterest));
    case 'mito'
        % Identify the mitochondrial reactions in all of the in silico lines
        rxnsOfInterest = [];
        for i = 1:noOfLines
            % Load model
            load([resultsFolder 'models' filesep inSilicoLines{i} filesep 'Model.mat'])
            compartmentReactions = findRxnFromCompartment(Model, '[m]');
            rxnsOfInterest = [rxnsOfInterest; compartmentReactions(:, 1)];
        end
        rxnsOfInterest = ismember(rxnFluxes.rxns, unique(rxnsOfInterest));
end

% Calculate the max length of the cluster or table
if sum(rxnsOfInterest) < 50
    maxLength = sum(rxnsOfInterest);
else
    maxLength = 50;
end

% Prepare the tables
inSilicoLinesBoolFluxes = ismember(rxnFluxes.Properties.VariableNames, inSilicoLines);
fluxesTable = rxnFluxes(find(rxnsOfInterest), :);
[fluxesTable, percentageDifferenceTable] = deal(fluxesTable(1:maxLength, :)); 
percentageDifferenceTable{:, inSilicoLinesBoolFluxes} = percentageDifferenceMean(1:maxLength, :);
percentageDifferenceTable.CTRLmean = [];
inSilicoLinesBoolChanges = ismember(percentageDifferenceTable.Properties.VariableNames, inSilicoLines);

% Cluster the fluxes of the most changed reactions
cgo = clustergram(table2array(percentageDifferenceTable(:, inSilicoLinesBoolChanges)), 'Colormap', redbluecmap);
cgo.Annotate = true;
set(cgo, 'RowLabels', cellstr(percentageDifferenceTable.rxnNames), 'ColumnLabels', inSilicoLines)
title = addTitle(cgo, {'Greatest percentage difference'; typeOfData});
savefig([resultsFolder typeOfData 'Cluster'])
f = figure('visible','off');
saveas(plot(cgo, f), [resultsFolder typeOfData 'Cluster'], 'png')
% Save a table with the information sorted
writetable(fluxesTable, [resultsFolder typeOfData 'Flux.csv'])
writetable(percentageDifferenceTable, [resultsFolder typeOfData 'Change.csv'])
